(ns readmarks.auth)

(defn authenticate [{:keys [username password]}]
  {:user_id  ::guest
   :username "John Doe"
   :roles    #{::guest}}
  #_(if-let [creds (get-credentials username password)]
    (let [userId (:user_id creds)
          roles (get-roles userId)]
      {:user_id userId
       :username (:name creds)
       :roles #{::admin}})))
