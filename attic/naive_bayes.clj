(ns readmarks.naive-bayes)

(defn init-counts [] {:counts {} :count 0})
(defn combine-freqs [freqs tokens]
  (if freqs
    (reduce (fn [freqs [token count]]
              (assoc freqs token (+ (get freqs token 0) count)))
      freqs tokens)
    tokens))
(defn update-counts [counts words]
  (-> counts
    (update-in [:counts] combine-freqs words)
    (update-in [:count] inc)))
(defn counts-to-p
  "Convert map values holding counts to normalized 

   XXX Note that returned values are not probabilities since counts 
   per document can be more than one if some word is repeated."
  [counter]
  (into {} (map (fn [[k v]] [k (/ (double v) (:count counter))]) (:counts counter))))


(defn init-freqs []
  {:word-freqs          (init-counts), ; word->count
   :category-word-freqs {}, ; category->{word->count}
   :category-freqs      (init-counts) ; category->count
   })

(defn set-category
  "Update category definitions with given document data."
  [freqs words category]
  (let [word-freqs (frequencies words)]
    (-> freqs
      (update-in [:word-freqs] update-counts word-freqs)
      (update-in [:category-word-freqs category] #(update-counts (or %1 (init-counts)) %2) word-freqs)
      (update-in [:category-freqs] update-counts {category 1}))))

(defn set-categories [freqs words categories]
  (reduce #(set-category %1 words %2) freqs categories))

(defn get-categories
  "Return list of most probable categories given accumulated data and document's word frequencies.
  p(c|w)=p(w|c)*p(c)/p(w) 
  where p(w)=sum[for all c] p(w|c)*p(c)"
  [freqs words]
  ; TODO refactor this code (maybe use some map-vals function?).
  (let [word-freqs (frequencies words)
        category-p (counts-to-p (:category-freqs freqs))
        category-word-p (filter (comp seq second) ; filter empty intersections 
                          (map (fn [[c p]] [c (select-keys (counts-to-p p) (keys word-freqs))])
                            (:category-word-freqs freqs)))
        category-and-word-p (map (fn [[c p]] [c (* (get category-p c) (reduce * (vals p)))]) category-word-p)
        sum-p (reduce + (map second category-and-word-p))]
    (map (fn [[c p]] [c (/ p sum-p)]) category-and-word-p)))
