(ns readmarks.layout
  (:use [hiccup.page :only [include-css include-js html5]])
  (:require [cemerick.friend :as friend]
            [readmarks.ui-helpers :as rmui]))

(defn cond-wrap [wrap? tag content]
  (if wrap? (conj tag content) content))

(defn cond-append [wrap? tag & content]
  (if wrap? (into tag content) tag))

(def login-uri "/auth/login")

(defn make-nav-link [link? uri name glyph hint]
  (if link?
    [:a.btn {:href uri :title hint} (rmui/icon-glyph glyph) "&nbsp;" name]
    [:span.btn {:title hint :disabled ""} (rmui/icon-glyph glyph) "&nbsp;" name]))

(def section-links {:link-list ["/links" "Search" :search "Search my links"]
                    :link-add ["/links/add" "Add" :plus "Store new link"]})

(def login-link [:a.btn.btn-success {:href login-uri} "Sign in"])

(defn make-header [home-link has-auth? page-id]
  (let [section-link (fn [[id [uri name glyph hint]]]
                       (make-nav-link (not= page-id id) uri name glyph hint))
        sections (if has-auth?
                   (map section-link section-links)
                   (if (not= page-id :login) [login-link]))]
    (cond-append
      (not (empty? sections))
      [:h3#page-header (cond-wrap (and home-link (not= page-id :home)) [:a {:href home-link}] "Readmarks")]
      " " (into [:span] sections))))

(defn page [params & content]
  (html5
    [:head
     [:title "Readmarks"]
     [:meta {:name "keywords" :content "URL, web, boormark, service, annotation"}]
     [:meta {:name "description" :content "Simple URL bookmarking service"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     ;(include-css "/css/normalize.css")
     (include-css "/css/bootstrap.css")
     (include-css "/css/main.css")]
    [:body
     [:div.container-fluid
      (let [auth (friend/current-authentication)]
        [:header (make-header (:home-link params) auth (:page-id params))
         (if auth
           [:div#profile-bar [:span.user [:a {:href "/auth/login"} (:email auth)]]
            [:a {:href "/auth/logout"} "Sign out"]]
           [:div#profile-bar
            [:a {:href login-uri} "Sign in"]])])
      [:div#content content]
      [:footer#page-footer "Author Petr Gladkikh &lt;PetrGlad at gmail com&gt;"]
      (include-js "/js/jquery.js")
      (include-js "/js/bootstrap.min.js")
      (include-js "/js/main.js")
      [:script "readmarks.main.main();"]]]))

(defn google-openid-notice []
  [:div {:class "bs-callout bs-callout-warning"}
   [:h4 "Google OpenId authentication note"]
   [:p "Google has ended support for OpenId authentication. I may support new Google auth in future.
        At the moment I encourage you to use user-password login instead."]
   [:p "Please contact me if you used Google's OpenId and still want to access your data."]])