# readmarks

Unsocial URL bookmarking service for information hoarders.

## Prerequisites

You will need [Leiningen][1] 2.0.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To run server 
    `lein run`

To change logging level pass JVM args
    `JAVA_OPTS='-Droot-level=INFO' lein ring server`

Example running standalone
    `java -cp target/readmarks-0.1.0-SNAPSHOT-standalone.jar clojure.main --main readmarks.server`

## Dev notes

Run nrepl server
    `lein repl :headless :port 59045`
Run debuggable nrepl server
    `JVM_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=15005" lein repl :headless`
    
Usual workflow:

 * Launch docker container petrglad/readmarks-data with scripts/run-db.sh 
   (see project readmarks-devops for docker images definitions).
 * Launch repl
 * In 'user' namespace invoke user/reset function to launch server
 * Invoke user/reset again to reload code changes        

## License

Public domain.
Author Petr Gladkikh <PetrGlad@gmail.com>, 2012, 2014, 2015

## ToDo

 * Show "This URL is already added." in non-bookmarklet case
 * Use bi-directional routing to reduce number of hard-coded url references
 * Export user data function
 * View cached version of page
 * Search in cached content 
 * Do not require http scheme in input while adding link (use ClojureScript and client-side validation)
 * Automatically refresh on anti-forgery-token failure  
 * Function to add other identities to current account (openid to user/password and vice-versa)
 * Support OAuth-based (OAuth-connect) authentication (needed for google login)

## Done
 
 * Nicer confirmation for link deletion
 * Digest re-fetch/retry button in link's details
 * Do not show links to current page (e.g. sign-in in sign-in)
 * Use nginx for ssl (see also devops project, https://github.com/dockerfile/nginx/blob/master/Dockerfile)
 * Refactor db structs to comply with clojure.jdbc requirements (remove {:datasource db} fragments) 
    (will reduce number of connections)
 * Search in link digest

