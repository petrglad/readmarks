create table usernames (
  user_id bigint references users(id),
  name varchar(64) unique primary key,
  password varchar(128) not null
);