(ns readmarks.honey-postgre-sql
  "PostgreSQL SQL generation for honeysql"
  (:require [honeysql.format :as hsf]))

;; Handle '@@' as infix operator; '@' is not allowed in a keyword - using other name
(defmethod hsf/fn-handler "ts-match" [_ ts-vector ts-query]
  (hsf/space-join [(hsf/format-predicate* ts-vector) "@@" (hsf/format-predicate* ts-query)]))

(defmethod hsf/fn-handler "cast" [_ expr type]
  (str "cast" (hsf/paren-wrap (str (hsf/paren-wrap (hsf/format-predicate* expr)) " as " (name type)))))



