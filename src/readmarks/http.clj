(ns readmarks.http
  (:use [ring.util.response :only [get-header]])
  (:require [clj-http.client :as client]))

(defn fetch [url]
  (client/get url
    ; see http://hc.apache.org/httpclient-legacy/preference-api.html
    {:as                      :string
     :http.socket.timeout     2000
     :http.connection.timeout 7000}))

(defn content-type [response]
  (get-header response "Content-Type"))