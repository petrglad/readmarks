(ns readmarks.executor
  (:import (java.util.concurrent Executors)))

(defn start []
  (Executors/newFixedThreadPool 2))

(defn stop [executor]
  (.shutdown executor))

(defn run-task [executor task]
  (.execute executor task))
