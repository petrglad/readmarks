(ns readmarks.admin.db-schema
  #_(:require [monger.query :as mq]
            [monger.collection :as mc]
            [monger.operators :as mo]
            [readmarks.db-util :as db-util])
  (:use [readmarks.logger :only [deflogger]]))

(deflogger)

(def metadata "meta")
(def documents "documents")
(def initial-version 0)

(def migrations
  {}
  #_{
    [0 1]
    (fn []
      ; Set surrogate ids on all documents
      (doseq [doc (-> (db-util/empty-query documents)
                      (mq/fields [:user-id :url])
                      mq/exec)]
        (let [user-id (:user-id doc)
              url (:url doc)]
          (mc/update documents
                     (db-util/restrict-query user-id {:url url})
                     {mo/$set {:id (db-util/surrogate-id user-id url)}})))
      (mc/ensure-index documents {:id 1} {:name "id" :unique true})
      (mc/ensure-index documents {:user-id 1 :url 1} {:name "primary" :unique true})
      #_(mc/ensure-index documents {:keywords 1})
      (mc/insert metadata
                 {:id      :db-schema
                  :version 1}))

    [1 2]
    (fn []
      (let [default-date (.getTimeInMillis (doto (java.util.Calendar/getInstance) (.set 2013, 0, 1)))]
        ; The query should be:
        ; db.documents.update({created: {$exists: false}}, {$set: {created: NumberLong(1357025704756)}}, {multi: true})
        (mc/update documents
                   {:created {mo/$exists false}}
                   {mo/$set {:created default-date}}
                   {:multi true}))
      (mc/ensure-index metadata {:id 1} {:name "id" :unique true}))})

#_(defn get-schema-version []
  (get (monger.collection/find-one-as-map metadata {:id :db-schema})
       :version
       initial-version))

(defn get-latest-schema-version []
  (reduce max (map second (keys migrations))))

(defn migrate! []
  (throw (RuntimeException. "Not implemented. Use lobos for migration.")) ; FIXME Use lobos for migration.
  #_(let [from (get-schema-version)
        to (get-latest-schema-version)]
    (if-let [migration (get migrations [from to])]
      (do
        (.info logger (str "Migrating to latest version: " from "->" to))
        (migration)
        (mc/update metadata {:id :db-schema} {mo/$set {:version to}} :multi false)
        (get-schema-version))
      (do
        (if (< from to)
          (.warn logger (str "No migration to latest version: " from "->" to)))
        nil))))
