create table openids (
  user_id bigint references users(id),
  openid varchar(256) not null,
  unique (openid)
);

insert into openids (user_id, openid)
  select id, openid from users;

alter table users drop column openid;



