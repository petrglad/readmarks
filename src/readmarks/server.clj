(ns readmarks.server
  (:require readmarks.handler
            readmarks.db
            ring.middleware.ssl
            readmarks.logger
            readmarks.executor
            [ring.adapter.jetty9 :refer [run-jetty]]
            ring.middleware.stacktrace
            [net.readmarks.compost :as compost])
  (:import (org.eclipse.jetty.server Server)
           (java.io Closeable)))

(readmarks.logger/deflogger)

(defn make-app-handler [context]
  (ring.middleware.stacktrace/wrap-stacktrace
    (readmarks.handler/app (select-keys context [:db :executor :session-store]))))

(defn make-system [config]
  {:data-source
   {:start (fn [_ _]
             (readmarks.db/init-db (:db config)))
    :stop (fn [this]
            (when this
              (.close ^Closeable this))
            nil)
    :get (fn [this]
           ;; Make structure that is expected in clojure.java.jdbc
           {:datasource this})}

   :executor
   {:start (fn [_ _]
             (readmarks.executor/start))
    :stop (fn [this]
            (when this
              (readmarks.executor/stop this))
            nil)}

   :server
   {:requires #{:data-source :executor}
    :start (fn [_ {executor :executor data-source :data-source}]
             (run-jetty
               (make-app-handler {:db data-source
                                  :executor executor
                                  :session-store (ring.middleware.session.memory/memory-store
                                                   (get config :session-store-atom (atom {})))})
               (merge
                 {:ssl? false
                  :max-threads 8
                  :min-threads 1}
                 (select-keys config [:port :join? :ssl]))))
    :stop (fn [this]
            (when this
              (.stop ^Server this))
            this)}})

(defn run
  "Start server, return system."
  [config-params]
  (readmarks.logger/set-uncaught-exception-handler logger)
  (.info logger "Starting server. Parameters: {}" config-params)
  (-> (make-system config-params)
    (compost/start #{:server})))

(defn stop
  "Should be called only once on given system."
  [system]
  (.info logger "Stopping server. System: {}" (keys system))
  (compost/stop system))
