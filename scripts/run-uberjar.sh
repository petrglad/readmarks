#!/bin/bash
set -e -x
THIS=$(dirname $0)
echo $(sudo docker exec -ti reks_data_dev env | grep READMARKS_DB_PASSWORD | sed -re 's/([^=]+)=(.+)/\2/')
cat $THIS/run-uberjar.edn
java  -Xms48M -Xmx96M \
  -cp "$THIS/../target/readmarks-0.2.3-SNAPSHOT-standalone.jar" \
  clojure.main --main readmarks.main \
  $THIS/run-uberjar.edn

