(ns readmarks.logger
  (:import (org.slf4j Logger)))

(defmacro deflogger []
  `(def ~(with-meta (symbol "logger") {:private true})
     (org.slf4j.LoggerFactory/getLogger ^String ~(str (ns-name *ns*)))))

(defn set-uncaught-exception-handler [^Logger logger]
  (Thread/setDefaultUncaughtExceptionHandler
    (reify Thread$UncaughtExceptionHandler
      (uncaughtException [_ thread throwable]
        (.error logger (str "Uncaught exception in thread " thread) throwable)))))