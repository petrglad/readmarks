(ns readmarks.db
  (:require readmarks.db-util)
  (:import (org.flywaydb.core Flyway)
           (com.zaxxer.hikari HikariDataSource)))

(defn open-db
  "Returns opened DataSource"
  [{url :url password :password}]
  (postgre-types.json/add-json-type
    readmarks.db-util/to-jdbc-json
    readmarks.db-util/from-jdbc-json)
  (doto (HikariDataSource.)
    ;(.setDataSourceClassName "org.postgresql.ds.PGPoolingDataSource")
    (.setJdbcUrl url)
    (.setUsername "readmarks")
    (.setPassword password)
    (.setMaximumPoolSize 5)))

(defn setup-db! [dataSource]
  (doto (Flyway.)
    (.setDataSource dataSource)
    (.migrate)))

(defn init-db [db-params]
  (let [db (open-db db-params)]
    (setup-db! db)
    db))

(defn with-db [dataSource f]
  (with-open [db (.getConnection dataSource)]
    (f db)))
