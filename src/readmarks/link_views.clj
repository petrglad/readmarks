(ns readmarks.link-views
  (:require hiccup.form
            [readmarks.modal-views :as modal]
            [readmarks.ui-helpers :as rmui]
            ring.middleware.anti-forgery
            ring.util.anti-forgery
            ring.util.request))

(defn bookmarklet-div [req]
  (let [hint "Drag this link onto bookmarks toolbar"]
    [:div.bookmarklet-bar
     (str hint " \u21D2 ")
     [:a {:href  (format
                   "javascript:void((function(){var w=window,d=document,wi=532,hi=435,top=(w.screenTop||w.screenY)+50,left=(w.screenX||w.screenLeft)+((w.innerWidth||d.documentElement.offsetWidth||0)/2)-(wi/2);var pop=function(){var url=encodeURIComponent(w.location.href),title=encodeURIComponent(d.title||'');var share=w.open('%s?url='+url+'&title='+title, '__readmarks_add', 'status=no,resizable=yes,scrollbars=no,personalbar=no,directories=no,location=yes,toolbar=no,menubar=no,width='+wi+',height='+hi+',top='+top+',left='+left);setTimeout(function(){share.focus()},50)};pop();})())"
                   (-> req
                     (assoc :uri "/links/add/bookmarklet")
                     (dissoc :query-string)
                     ring.util.request/request-url))
          :title hint}
      "ReadMark"]]))

(defn link-add-form [url add-forms]
  (into
    [:div.link-url-form
     [:form {:action "/links", :method :post}
      (ring.util.anti-forgery/anti-forgery-field)
      [:div.input-group
       [:span.input-group-btn
        [:button.btn.btn-default {:type :submit, :name :add :title "Add link"} (rmui/icon-glyph :plus)]]
       [:input.form-control
        {:type        :url,
         :name        :url,
         :required    :required,
         :autofocus   :autofocus,
         :placeholder "Add URL...",
         :value       (or url "")}]]]]
    add-forms))

(defn link-filter-form [query]
  [:divcollection
   [:form.link-url-form {:action "/links" :method :get}
    [:div.input-group
     [:span.input-group-btn
      [:button.btn.btn-default {:type :submit} (rmui/icon-glyph :search)]]
     [:input.form-control
      {:type        :text,
       :name        :q,
       :autofocus   :autofocus,
       :placeholder "Search...",
       :value       query}]]]])

(defn link-list-view [{query :query limit :limit}]
  [:div
   (link-filter-form query) ; TODO (UI, improvement) Lookup links dynamically, improve validation feedback (automatically add http scheme)
   ; XXX Probably there's better way to pass af-token
   [:div#link-list {:data-anti-forgery-token ring.middleware.anti-forgery/*anti-forgery-token* ; Content is attached at client side
                    :data-query              query
                    :data-limit              limit}]])

(defn make-list [tag item-tag items]
  (into [tag] (map (fn [item] [item-tag item]) items)))

(defn make-inline-list [items]
  (make-list :ul.list-inline :li items))

(defn link-details-view [link-doc]
  (let [digest (:digest link-doc)
        headings (:headings digest)]
    [:div
     [:div.row-controls
      [:form {:action (str "/links/" (:id link-doc) "/update") :method :post}
       (ring.util.anti-forgery/anti-forgery-field)
       [:button.btn.btn-primary {:type :submit :title "Refresh cached information for this URL"} (rmui/icon-glyph :refresh)]]
      (let [url (:url link-doc)]
        [:a {:href url} url])]

     ; [:a {:href (str "/links/" (:id link-doc) "/cached")} "See cached version"] ; View cached version

     [:section
      [:h3 "Headings"]
      (make-list :ul.list-unstyled :li
        (map
          (fn [hlevel] (make-inline-list (second hlevel)))
          (filter (comp seq second) headings)))]

     [:section
      [:h3 "Tags"]
      (make-inline-list (:tags digest))]

     (if-let [html-keywords (seq (:keywords digest))]
       [:section
        [:h5 "HTML keywords"]
        (make-inline-list html-keywords)])]))