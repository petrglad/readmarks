(ns readmarks.db-util
  (:require postgre-types.json
            readmarks.honey-postgre-sql
            [clojure.data.json :as json]
            [clojure.java.jdbc :as jdbc])
  (:import org.postgresql.util.PGobject))

(defn as-pg-object [type-name value]
  (doto (PGobject.)
    (.setType (name type-name))
    (.setValue value)))

(defn to-jdbc-json [value]
  (let [value-writer (fn [_key value]
                       (if (= java.util.Date (type value))
                         (.format (java.text.SimpleDateFormat. "yyyy-MM-dd'T'HH:mm:ssZ") value)
                         value))]
    (json/write-str value :value-fn value-writer)))

(defn from-jdbc-json [value]
  (when value
    (json/read-str value :key-fn keyword)))

#_(defn surrogate-id [& fields]
  (->
    (apply str (interpose "|" fields))
    org.apache.commons.codec.digest.DigestUtils/sha256
    org.apache.commons.codec.binary.Base64/encodeBase64URLSafeString))

(defn do-return-keys [db sql-args]
  (jdbc/db-do-prepared-return-keys db sql-args))
