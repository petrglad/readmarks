(ns readmarks.routes
  (:require [clojure.string :as ss]
            [cemerick.url :as url]))

(def links-path "/links")

(def auth-path "/auth")
(def auth-new-user-path (str auth-path "/newuser"))
(def auth-login-path (str auth-path "/login"))

(def api-path "/r")
(def api-links-path (str api-path links-path))

(defn uri [path query]
  (ss/join "?" [path (url/map->query query)]))

(defn to-link [link-id]
  (str links-path "/" link-id))

(defn to-link-list [query]
  (uri api-links-path {:q query}))
