(ns readmarks.modal-views)

(defn add-modal-trigger
  "See http://getbootstrap.com/javascript/#modals"
  [dialog-id element]
  {:pre (map? (get element 1))}
  (assoc element
    1
    {:data-toggle "modal"
     :data-target (str "#" dialog-id)}))

(defn make-confirm-dialog
  "See http://getbootstrap.com/javascript/#modals"
  [title content confirmActionTitle]
  (let [dialog-id (gensym "dlg-")
        modal-label (gensym "label-")]
    [:div.modal.fade {:id dialog-id :tabindex -1 :role "dialog" :aria-labelledby modal-label :aria-hidden "true"}
     [:div.modal-dialog
      [:div.modal-content
       [:div.modal-header
        [:button.close {:type "button" :data-dismiss "modal" :aria-label "Close"}
         [:span {:aria-hidden "true"} "&times;"]]
        [:h4.modal-title {:id modal-label} title]]
       [:div.modal-body content]
       [:div.modal-footer
        [:button.btn.btn-default {:type "button" :data-dismiss "modal"} "Dismiss"]
        [:button.btn.btn-primary {:type "button"} confirmActionTitle]]]]]))

