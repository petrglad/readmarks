(ns cs-repl
  (:require [cljs.repl :as repl]
           [cljs.repl.browser :as browser])) ;; browser implementation of IJavaScriptEnv

#_(defn repl []
  (def env (browser/repl-env)) ;; create a new environment
  (repl/repl env)) ;; start the REPL

