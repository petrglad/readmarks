(ns readmarks.links
  (:require [honeysql.core :as sql]
            [honeysql.helpers :as hsh]
            ring.util.request
            [clojure.java.jdbc :as jdbc]
            ring.util.anti-forgery
            clojure.string
    ;[clojure.core.typed :as t]
            readmarks.logger
            [readmarks.db-util :as db-util]
            readmarks.analyse
            [crypto.password.pbkdf2 :as password-hash]
            [schema.core :as scm])
  (:import [clojure.lang Keyword]))

(readmarks.logger/deflogger)

; ---------------------------------------------------------
; Users

; TODO (refactoring) Move user manipulation code to separate namespace

(defn- create-user [db]
  (:id (readmarks.db-util/do-return-keys db
         (sql/format {:insert-into :users
                      :values [{:id :default}]}))))

(defn add-openid [db user-id openid]
  (:user_id (first (jdbc/insert! db :openids
                     {:user_id user-id
                      :openid openid}))))

(defn find-openid [db openid]
  (:user_id (first
              (jdbc/query db
                (sql/format {:select [:user-id]
                             :from [:openids]
                             :where [:= :openid openid]})))))

(defn create-user-openid [db openid]
  (jdbc/db-transaction* db
    (fn [conn]
      (add-openid conn (create-user conn) openid))))

(defn find-user-by-username [db username]
  (first (jdbc/query db
           (sql/format {:select [:user-id :name :password]
                        :from [:usernames]
                        :where [:= :name username]}))))

(defn add-username [db user-id {:keys [:username :password]}]
  (jdbc/db-transaction* db
    (fn [conn]
      (if (find-user-by-username conn username)
        nil
        (:user_id (first
                    (jdbc/insert! conn :usernames
                      {:user_id user-id
                       :name username
                       :password (password-hash/encrypt password)})))))))

(defn create-user-u-p [db up-credentials]
  (jdbc/db-transaction* db
    (fn [conn]
      (add-username conn (create-user conn) up-credentials))))

(defn find-user-by-credentials [db {:keys [:username :password] :as creds}]
  (.info logger "Got credentials {}" creds)
  (let [user (find-user-by-username db username)]
    (if (and user (password-hash/check password (:password user)))
      {:user-id (:user_id user)}
      (.info logger "No match for credentials {}" creds))))

(defn user-by-openid [db {openid-str :identity :as openid-struct}]
  (.info logger "Got openid {}" openid-struct)
  (when (clojure.string/blank? openid-str)
    (throw (IllegalStateException. "User is not authenticated")))
  (jdbc/db-transaction* db
    (fn [conn]
      (let [user-id (find-openid conn openid-str)]
        {:user-id
         (if user-id
           user-id
           (create-user-openid conn openid-str))}))))

; ---------------------------------------------------------
; Links

(def LinkSchema {:id scm/Int
                 :url scm/Str
                 (scm/optional-key :digest) (scm/maybe {Keyword scm/Any})})

(defn check-user-id [user-id]
  (assert (< 0 user-id) "User id is required"))

(defn owner-restriction
  "Condition to be applied to all link queries"
  [user-id]
  (check-user-id user-id)
  [:= :user_id user-id])

(defn restrict-query [q user-id]
  (hsh/merge-where q (owner-restriction user-id)))

(defn qualify-keyword [prefix kw]
  (keyword (str (name prefix) "." (name kw))))

(defn make-link-query [extra-fields]
  {:select (map (partial qualify-keyword :links)
             (into [:id :url] extra-fields))
   :from [:links]})

(defn do-link-query [db user-id query]
  (jdbc/query db
    (sql/format (restrict-query query user-id))))

(defn link-list [db user-id query limit]
  (do-link-query db user-id
    (cond-> (make-link-query [:digest])

      (not (clojure.string/blank? query))
      (hsh/merge-where
        (if (<= 0 (.indexOf query "%"))
          [:like :url query]
          [:ts-match :keywords
           [:cast (clojure.string/join " & " (readmarks.analyse/split-keywords query)) :tsquery]]))

      true
      (assoc :limit limit)

      true
      (assoc :order-by [[:links.id :desc]]))))

(defn link-get [db user-id url]
  (first
    (do-link-query db user-id
      (hsh/merge-where (make-link-query [:digest]) [:= :url url]))))

(defn link-get-by-id [db user-id id extra-fielads]
  {:pre [(integer? id) (sequential? extra-fielads)]
   :post [(or (nil? %) (scm/validate LinkSchema %))]}
  (first
    (do-link-query db user-id
      (hsh/merge-where (make-link-query extra-fielads) [:= :id id]))))

(defn link-add! [db user-id url]
  (check-user-id user-id)
  (.debug logger "Adding link {}/{}" user-id url)
  (:id (or
         (link-get db user-id url)
         (first
           (jdbc/insert! db :links
             {:user_id user-id :url url})))))

(defn link-id-where [user-id id]
  {:pre (< 0 id)}
  [:and (owner-restriction user-id) [:= :id id]])

(defn db-execute! [db sql-map params]
  (jdbc/execute! db
    (sql/format sql-map params)
    {:transaction? true}))

(defn link-delete! [db user-id id]
  (check-user-id user-id)
  (.debug logger "Delete link {}:{}" user-id id)
  (db-execute! db
    (-> (hsh/delete-from :links)
      (hsh/where (link-id-where user-id id)))
    {}))

(defn link-update! [db user-id id digest-result]
  {:pre (<= 0 id)}
  (check-user-id user-id)
  (.debug logger "Update link {}:{}" user-id id)
  (.debug logger "Update link {} with keywords {}" id (clojure.string/join " " (:keywords digest-result)))
  (db-execute! db
    (-> (hsh/update :links)
      (hsh/sset {:cache    :?cache
                 :digest   :?digest
                 :keywords :?keywords})
      (hsh/where (link-id-where user-id id)))
    {:cache    (:cache digest-result)
     :digest   (:digest digest-result)
     :keywords (db-util/as-pg-object "tsvector"
                 (clojure.string/join " " (:keywords digest-result)))}))
