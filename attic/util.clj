(ns readmarks.util
  (:require readmarks.spider
           clojure.edn))

; ---------------------------------------------------------
; Sample data

(defn fetch-test-sample [url pages-root]
  (let [outfile (clojure.java.io/file (str pages-root "/" (clojure.string/replace url #"\W+" "_") ".edn"))
        skip? (.isFile outfile)]
    (if (not skip?) (spit outfile (readmarks.spider/fetch-docs url)))
    {:url url :file outfile :skipped skip?}))

(defn get-cached-samples [pages-root]
  (->> (file-seq (clojure.java.io/file pages-root))
    (filter #(.isFile %))
    (map #(try
            (clojure.edn/read-string (slurp %))
            (catch Exception e
              (throw {:exception e, :file %}))))))

; Gather sample data
#_(def sample-pages-root "dev-resources/test-data/pages")
#_(map fetch-test-sample 
     ["http://pedestal.io/"
      "http://clojuremadesimple.co.uk/"])
