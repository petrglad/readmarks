(ns readmarks.main
  (:use [readmarks.server :only [run stop]])
  (:require environ.core
            [clojure.java.io :refer [input-stream]]
            readmarks.logger)
  (:gen-class))

(readmarks.logger/deflogger)

(defn get-env [properties-file]
  (if properties-file
    (merge environ.core/env (read-string (slurp properties-file)))
    environ.core/env))

(defn add-shutdown-hook [hook]
  (.warn logger "Adding shutdown hook")
  (.addShutdownHook (Runtime/getRuntime)
    (doto (Thread. ^Runnable hook)
      (.setName "shutdown-hook"))))

(defn -main [properties-file]
  (readmarks.logger/set-uncaught-exception-handler logger)
  (try
    (.info logger (str "Additional properties file is " properties-file))
    (let [env (get-env properties-file)
          db-host (env :db-host "db")
          db-url (env :db-url (str "jdbc:postgresql://" db-host ":5432/readmarks"))
          password-env-var "readmarks-db-password"
          db-password (env (keyword password-env-var)
                        ; (!) This env var is provided by Docker
                        (env (keyword (.toLowerCase (str db-host "-env-" password-env-var)))))]
      (.debug logger "ENV: {}" env)
      (let [started (run {:port (Integer/parseInt (env :readmarks-port "8080"))
                          :join? false
                          :db {:url db-url
                               :password db-password}
                          :keystore (env :keystore)
                          :key-password (env :keystore-password)
                          :ssl-port (Integer/parseInt (env :readmarks-ssl-port "8443"))})]
        (add-shutdown-hook #(stop started))))
    (catch Throwable e
      (.error logger "Exception caught at top level." e))))
