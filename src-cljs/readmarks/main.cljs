(ns readmarks.main
  (:require [reagent.core :as reagent]
            [jayq.core :as jq]
            ;[jayq.util :as jqu]
            ;[clojure.browser.dom :as dom]
            ;[clojure.browser.repl :as repl]
            [clojure.string :as ss]
            [readmarks.ui-helpers :as rmui]
            [readmarks.routes :as routes]
            [cljs-hash.goog :as gh]))

; (repl/connect "http://localhost:9000/repl")


; ---------------------------------------------------------
; Common code

(defn log [msg]
  (.log js/console msg))

(defn ajax [uri params done fail]
  (-> (jq/ajax uri (merge {:method :get} params))
    (jq/done done)
    (jq/fail fail)))

(defn alert-fn [message]
  (fn [_] (js/alert (str message))))

(defn toggle [on]
  (if on jq/show jq/hide))

(defn delete-link [link-id af-token links]
  (ajax (routes/to-link link-id)
    {:method :delete
     :headers {:x-xsrf-token af-token}}
    (fn [] (swap! links (fn [old] (remove #(= link-id (:id %)) old))))
    (alert-fn "Deletion failed.")))

(defn dismissable-popup [on-close content]
  (let [id (gensym)]
    (reagent/create-class
      {:display-name
       "dismissable-popup"

       :component-did-mount
       (fn []
         (.focus (jq/$ (str "#" id " .btn-info"))))

       :reagent-render
       (fn [on-close content]
         (into
           [:div.confirmation-popup.modal-content
            {:id        id
             :on-key-up (fn [event]
                          (when (= 27 (.-keyCode event))
                            (on-close)))
             :on-blur   (fn [event]
                          (when (-> (js/$ (.-relatedTarget event))
                                  (.closest ".confirmation-popup")
                                  .-length
                                  (= 0))
                            (on-close)))}]
           content))})))


(defn confirmation-popup [on-close on-confirm]
  [dismissable-popup on-close
   [[:button.btn.btn-xs.btn-info {:on-click on-close} "Keep"]
    [:button.btn.btn-xs.btn-danger {:on-click #(do (on-confirm) (on-close))} "Delete"]]])


; ---------------------------------------------------------
; Link list

(defn delete-link-dlg [on-close action]
  (confirmation-popup on-close action))

(defn delete-link-button [link-id af-token links]
  (let [deleting? (reagent/atom false)]
    (fn []
      [:span
       [:button.btn.link-list-action.btn-xs.btn-danger
        {:title    "Delete link "
         :on-click #(swap! deleting? not)}
        (rmui/icon-glyph :remove)]
       (when @deleting?
         [:div.delete-confirmation {:style {:display :inline-block}}
          (delete-link-dlg
            #(reset! deleting? false)
            #(delete-link link-id af-token links))])])))

(defn link-annotation-view [link-digest]
  [:div.link-item-keywords
   (->> (concat
          (take 10 (apply concat (vals (:headings link-digest))))
          (take 20 (map (fn [kw]
                          [:a {:href (routes/uri routes/links-path {:q kw})} kw])
                     (:tags link-digest))))
     (map-indexed (fn [idx tag]
                    [:span.keyword {:key idx} tag]))
     (interpose " "))])

(defn link-list-item [af-token links {url :url digest :digest id :id}]
  ^{:key id}
  [:li.link-item
   [:a.link-item-href {:href url} url]
   [:a.link-list-action {:href (str routes/links-path "/" id) :title "Link details"}
    (rmui/icon-glyph :cog)]
   [delete-link-button id af-token links]
   (link-annotation-view digest)])

(defn link-list [af-token links]
  ; (if (<= limit (count links)) [:small (str "Shown only " limit " most recent links.")]) ; TODO (UI, improvement) Show this warning or add "more" button
  (into [:div {}]
    (mapv (partial link-list-item af-token links) @links)))

(defn get-links [query on-success]
  (ajax (routes/to-link-list query) {}
    (fn [result]
      (on-success (js->clj result :keywordize-keys true)))
    (alert-fn "Server error")))

(defn setup-link-list [el query af-token]
  (let [links (reagent/atom [])
        refresh (fn [] (get-links query #(reset! links %)))]
    (refresh)
    (reagent/render [link-list af-token links] el)))

(defn attach-to-link-list []
  (doseq [el (jq/$ "#link-list")]
    (setup-link-list el
      (aget (.-dataset el) "query")
      (aget (.-dataset el) "antiForgeryToken"))))


; ---------------------------------------------------------
; Login form

(defn value-form-group [value errors label input-attrs]
  ;;; TODO (refactoring) Too much parameters for this function, use map instead?
  (let [control-id (gensym)]
    (fn []
      (let [messages-list (remove nil? (errors))
            change-handler (fn [event]
                             ;;; DEBUG (log (str "change: " (-> event .-target .-value)))
                             (reset! value (-> event .-target .-value)))]
        [:div.form-group {:class (if-not (empty? messages-list) "has-warning")}
         [:label {:for control-id} label]
         [:input.form-control (-> input-attrs
                                (assoc :id control-id)
                                (assoc :value @value)
                                (assoc :on-change change-handler))]
         (map (fn [message-text]
                [:p.text-warning {:key (gh/md5-hex message-text)} message-text])
           messages-list)]))))

(defn submit-button [caption enabled?]
  [:input.btn.btn-success
   {:type "submit"
    :value (caption)
    :disabled (if-not (enabled?) "disabled")}])

(defn login-form [login-error? username-taken? username-str]
  (let [login-error? (reagent/atom login-error?)
        is-new-user? (reagent/atom username-taken?) ;; Taken username implies new user
        username (reagent/atom username-str)
        username-errors (fn []
                          [(if (ss/blank? @username)
                             "User name is required."
                             (when (and @is-new-user?
                                        username-taken?
                                        (= username-str @username))
                               (str "User name '" @username "' is already taken.")))])
        password (reagent/atom "")
        password-errors (fn []
                          [(if (= 0 (count @password))
                             (str "Password is required."))])
        password2 (reagent/atom "")
        password2-errors (fn []
                           [(if @is-new-user?
                              (if (= 0 (count @password2))
                                (str "Password confirmation is required.")
                                (if (not= @password @password2)
                                  "Password confirmation does not match.")))])
        can-submit? (fn []
                      (log (str (concat (username-errors) (password-errors) (password2-errors))))
                      (empty? (remove nil? (concat (username-errors) (password-errors) (password2-errors)))))]
    (fn []
      [:form#loginForm {:method "POST"
                        :action (if @is-new-user?
                                  routes/auth-new-user-path
                                  routes/auth-login-path)}
       (when @login-error?
         (reset! login-error? false)
         [:p.text-warning "Name and password do not match records."])
       ;;; TODO (improvement) Require username of length at least 2 and password at least 6
       ;;; TODO (improvement) Trim user name before submitting
       ;;; TODO (usability, bug) Autofill user/passwords may not trigger change events so login button stays disabled until user focuses the form.
       [(value-form-group username username-errors
          "User name"
          {:type "text" :name "username"
           :autoFocus :autofocus
           :autoComplete :username
           :required "required"
           :defaultValue username-str})]
       [(value-form-group password password-errors
          "Password"
          {:type "password"
           :name "password"
           :required "required"
           :autoComplete :current-password})]
       [:div.checkbox {:title "Check this to create new account at Readmarks."}
        [:label
         [:input#isNewUser {:type "checkbox"
                            :name "isNewUser"
                            :checked @is-new-user?
                            :on-change (fn []
                                         (swap! is-new-user? not))}]
         "I am new user here"]]
       (when @is-new-user?
         [(value-form-group password2 password2-errors
            "Same password again"
            {:type "password"
             :name "password2"
             :autoComplete :off
             :autoFocus true})])
       [submit-button #(if @is-new-user? "Register" "Login") can-submit?]])))

(defn attach-to-login-form []
  (if-let [login-ui-el (first (jq/$ "#passwordLoginUi"))]
    (let [data (.-dataset login-ui-el)]
      (reagent/render
        [(login-form
           (boolean (aget data "loginError"))
           (boolean (aget data "usernameTaken"))
           (aget data "username"))]
        login-ui-el))))


; ---------------------------------------------------------

(defn ^:export main []
  (attach-to-link-list)
  (attach-to-login-form))
