(ns ^{:name "#{Google, Yahoo, AOL, Wordpress, +} via OpenID"
      :doc "Using OpenID to authenticate with various services."}
readmarks.auth.login
  (:use [ring.util.anti-forgery :only [anti-forgery-field]])
  (:require [cemerick.friend :as friend]
            [cemerick.friend.openid :as openid]
            [compojure.core :refer (GET defroutes)]
            [ring.util.response :as resp]
            [hiccup.page :as h]
            hiccup.form
            readmarks.layout))

; TODO Rename this ns to "readmarks.auth.login"

; Slightly modified code from cemeric/friend_demo cemerick.friend-demo.openid

(def providers [; Google does not support OpenId since 2015-04-20:
                ; {:name "Google" :url "https://www.google.com/accounts/o8/id"}

                {:name "Yahoo" :url "http://me.yahoo.com/"}
                {:name "AOL" :url "http://openid.aol.com/"}])

(defn account-info [auth]
  [:p "Your account information"
   [:dl.dl-horizontal
    (apply concat
      (for [[k v] auth
            :let [[k v] (if (= :identity k)
                          ["OpenID identity" (if (nil? v) "[none]" (str (subs v 0 (* (count v) 2/3)) "…"))]
                          [k v])]]
        [[:dt (name k)] [:dd (str v)]]))]])

(defn login-forms [{params :params}]
  [:div
   [:h3 "Login"]
   [:div.row
    [:div#passwordLoginUi.col-sm-6.col-md-4
     {:data-login-error (:login_failed params)
      :data-username-taken (:username-taken params)
      :data-username (:username params)}]]
   [:div.row
    [:div.col-sm-6.col-md-4
     " or login with "
     (for [{:keys [name url]} providers
           :let [dom-id (str (gensym))]]
       [:form.form-inline.openid-button {:method "POST"
                                         :action "/auth/openid"}
        [:input {:type "hidden" :name "identifier" :value url :id dom-id}]
        [:input.btn {:type "submit" :value name}]])

     #_[:p "…or, with an OpenID URL"]
     #_[:form.form-inline.input-append {:method "POST" :action "/auth/openid"}
      [:input {:type "text" :name "identifier" :style "width:250px;"}]
      [:input.btn {:type "submit" :value "Login"}]]]]
   #_[:div.row (readmarks.layout/google-openid-notice)]])

(defn login-page [req]
  [:div
   (if-let [auth (friend/current-authentication req)]
     (account-info auth)
     (login-forms req))])
