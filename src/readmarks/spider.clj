(ns readmarks.spider
  (:require [clojure.zip :as zip]
            readmarks.http
            readmarks.links
            clojure.string
            readmarks.analyse
            crouton.html
            readmarks.logger))

(readmarks.logger/deflogger)

(defn digest-url [url]
  (.info logger "Fetching {}" url)
  (let [fetched (readmarks.http/fetch url)
        parser (let [content-type (readmarks.http/content-type fetched)]
                 (cond
                   (.startsWith content-type "text/html") readmarks.analyse/digest-html
                   (.startsWith content-type "text/") readmarks.analyse/digest-text
                   true (constantly {:message (str "Unsupported content type " content-type)})))
        digest (parser (java.io.StringReader. (:body fetched)))]
    {:cache    fetched
     :digest   digest
     :keywords (readmarks.analyse/keyword-set digest url)}))

; http://www.ibm.com/developerworks/library/j-treevisit/index.html
; (map #(-> % zip/node) (take-while (complement zip/end?) (iterate zip/next (html-zipper h))))

#_(defn html-zipper [root]
  (zip/zipper
    (or #(map? %) #(vector? %))
    :content
    #(assoc %1 :content (apply vector %2))
    root))

#_(defn header [html-struct]
  (-> (html/select html-struct [:h1])
    first
    :content))

#_(defn get-frame-urls [body]
  (->> (html/select body [:frame])
    (map #(first (html/attr-values % :src)))
    (filter identity))) ; filter frames without src attr

#_(defn fetch-doc-data [url]
  (try
    (fetch-parsed url)
    (catch clojure.lang.ExceptionInfo e
      ; XXX Expecting HTTP error (for other exceptions object field may hold different data)
      (-> (.getData e) :object (dissoc :body)))
    ; Exception types: org.apache.http.HttpException java.io.IOException NumberFormatEsception
    (catch Exception e
      {:status    :exception
       :exception {:type    (type e)
                   :message (.getMessage e)}})))

#_(defn fetch-docs
  "Fetch html from url. Iframes - separate documents."
  [url]
  (let [root-doc (fetch-doc-data url)
        result {url root-doc}]
    (if (:body url)
      ; Note that only first level frames are loaded
      (let [frame-urls (get-frame-urls (:body root-doc))]
        (into result
          (pmap #(vector % (fetch-doc-data %)) frame-urls)))
      result)))
