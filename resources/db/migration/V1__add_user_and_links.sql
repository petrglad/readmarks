create table users (
  id bigserial primary key,
  openid varchar(256) not null unique
);

create table links (
  id bigserial primary key,
  user_id bigint references users(id) not null,
  url varchar(4096) not null,

  unique (user_id, url)
);