(ns readmarks.ui-helpers
  "Client and server shared code")

(defn icon-glyph
  "See http://getbootstrap.com/components/"
  [icon-id]
  [:span {:class (str "glyphicon glyphicon-" (name icon-id)) :aria-hidden "true"}])
