(ns readmarks.test.naive-bayes
  (:use clojure.test
        readmarks.naive-bayes))

(deftest counts-to-p-test
  (is (= {:a 8}
        (-> (init-counts)
          (update-counts {:a 8})
          (counts-to-p))))
  (is (= {:a 5 :b 1}
        (-> (init-counts)
          (update-counts {:a 8})
          (update-counts {:a 2 :b 2})
          (counts-to-p)))))

(deftest bayes-test
  (let [stats (-> (init-freqs)
                (set-categories ["poetry" "expressiveness"] [:humanities])
                (set-category ["clojure" "expressiveness" "expressiveness"] :clojure))]

    (is (= [] (get-categories stats ["gizmodo"])))
    (is (= [[:humanities 1]] (get-categories stats ["poetry"])))
    (is (= [[:clojure 1]] (get-categories stats ["clojure" "programming"])))
    (is (let [cs (into {} (get-categories stats ["clojure" "programming" "explained" "expressiveness"]))]
          (and
            (= #{:clojure :humanities} (into #{} (keys cs)))
            (> (:clojure cs) (:humanities cs)))))))
