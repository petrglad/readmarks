(ns user
  (:use [clojure.tools.namespace.repl :only (refresh)]
        [clojure.repl]
        [clojure.java.shell :only [sh]]
        [clojure.reflect :only [reflect]]
        [cemerick.pomegranate :only (add-dependencies)])
  (:require [clojure.java.classpath :as cp]
            [clojure.tools.trace :as trace]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer (pprint)]
            [clojure.tools.analyzer.jvm :as ajv]))

(def debug-server-params
  {:port               8090
   :join?              false
   :db                 {:url      "jdbc:postgresql://127.0.0.1:15432/readmarks"
                        :password "44j2LoVpRruDOwth3tSvs/NDxXCQer5TLAPrahNe"}
   :session-store-atom (atom {})}) ; Keeps sessions between restarts

(defonce server (atom nil))

(defn in-server
  "Dynamically resolve ns in server namespace.
   Lazy loads to have 'user namespace in case of server compilation errors."
  [sym]
  (require 'readmarks.server)
  (if-let [sns (find-ns 'readmarks.server)]
    (ns-resolve sns sym)
    (throw (IllegalStateException. "Cannot find namespace readmarks.server"))))

(defn start-server []
  (swap! server
    (fn [s]
      (when s
        ((in-server 'stop) s))
      ((in-server 'run) debug-server-params))))

(defn stop-server []
  (swap! server
    (fn [s]
      (when s
        ((in-server 'stop) s)))))

; To start local dev DB server: docker run --rm -p 15432:5432 --name=reks_data_dev petrglad/readmarks-data

(defn reset []
  (stop-server)
  (refresh :after 'user/start-server))

(defn db []
  {:datasource (:data-source server)})
