(ns readmarks.import
  (:require [net.cgrand.enlive-html :as html]
            [crouton.html :as crouton]
            [readmarks.util]
            [readmarks.spider]
            [clojure.java.io :as jio]
            [clj-http.client :as client]
            [readmarks.naive-bayes :as bayes]
            [readmarks.analyse]))

(defn parse-delicious
  "Returns sequence of [url tags] given HTML with exported delicious bookmarks."
  [input-reader]
  (let  [dls (crouton/parse input-reader)]
    (->> (html/select dls [:a])
      (map #(let [attrs (:attrs %)]
              [(:href attrs)
               (clojure.string/split (:tags attrs) #",")])))))


; -------------------------------------------
; Lab


#_(def pg-download-cache "lab/data/pg-delicious")
#_(def pg-delicious (parse-delicious (jio/reader "/home/petr/Downloads/delicious.html")))

#_(defn cache-delicious-links []
  ; download all links
  (do ; TODO use connection manager client/with-connection-manager
    (map #(readmarks.util/fetch-test-sample % pg-download-cache)
         (map first pg-delicious))))

#_(doseq [result (cache-delicious-links)]
    (prn result))

; TODO train bayes on [url [tags...]] from parsed delicious and from cached pages
; TODO (low) convert all statuses (http and exceptions) to single form to simply determine success/failure
; TODO (low) add support for plain text urls
; TODO add tokens from metadata and url (in addition to word tokens)
; TODO implement OpenId and user separation
; TODO store user data (bayes and docs) in DB

#_(defn estimate-bayes []
  (let [tags (into {} pg-delicious)]
    (->> (readmarks.util/get-cached-samples pg-download-cache)
      (reduce merge)
      ; TODO Docs should be grouped by "buckets" frames belonging to same page probably should be counted together
      (group-by #(:status (second %)))
      (#(get % 200)) ; only successfuly downloaded docs
      (map (fn [[url doc]] [url (readmarks.analyse/get-words (:body doc))]))
      (reduce (fn [freqs [url words]]
                (bayes/set-categories freqs words (get tags url)))
              (bayes/init-freqs)))))

;(bayes/get-categories pg-dl-freqs ["clojure"])
;(time (take 5 (sort-by (comp - second) (bayes/get-categories pg-dl-freqs ["copyright"]))))