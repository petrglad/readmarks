; Document properties extraction
(ns readmarks.analyse
  (:require [net.cgrand.enlive-html :as html]
            [clojure.string :as cstr]
            [net.cgrand.xml :as xml]))

(defn attr-splitted
  "Modified version of net.cgrand.enlive-html/attr-values"
  [node attr separator]
  (when-let [v (-> node :attrs (get attr))]
    (set (clojure.string/split v separator))))

(defn groom-keywords [keywords]
  (remove empty? (map #(cstr/replace (cstr/trim %) #"\s+" " ") keywords)))

(defn get-keywords [html]
  (->> (html/select html [[:meta (html/attr= :name "keywords")]])
    (map #(attr-splitted % :content #"\s*,\s*"))
    first
    groom-keywords))

(defn get-words [body]
  (->> (html/select body [(html/but [:script]) :> html/text-node])
    (mapcat #(clojure.string/split % #"\s+"))
    (filter #(re-matches #"\w\S+" %))))


; -------------------------------------------------
; Lab


; TODO Probably better to learn these words from actual texts; That would support different languages - currently for English only.
; Or use existing library for natural language processing
(def common-aux-words
  #{"the"
    "and"
    "of"
    "to"
    "a"
    "that"
    "in"
    "is"
    "i"
    "it"
    "for"
    "as"
    "have"
    "has"
    "on"
    "at"
    "was"
    "are"
    "they"
    "s" ; "*'s"
    "with"
    "he"
    "an"
    "but"
    "been"
    ;"more"
    "not"
    "be"
    "or"
    "t" ; "*n't"
    "said"
    ;"like"
    "from"
    "which"
    "there"
    ;"one"
    ;"about"
    "were"
    "had"
    "by"
    "you"
    ;"most"
    ;"many"
    "can"
    "But"
    "what"
    "than"
    "no"
    "my"
    "all"
    "It"
    "into"
    "who"
    "when"
    "their"
    "so"
    "out"
    "his"
    "then"
    "become"
    ;"vital"
    ;"study"
    "say"
    ;"food"
    "also"
    "we"
    "this"
    "me"
    "these"
    "if"
    ;---------
    "them"
    "do"
    "one"
    "about"
    "most"
    "like"
    "get"
    "us"
    "just"
    "more"
    "our"
    "up"
    "much"

    "doesn" ; "doesn't"
    "don" ; "don't"
    "ve" ; "*'ve"
    "re" ; "*'re"
    "ll" ; "*'ll"
    })

(defn str-as-html [s]
  (html/html-resource (java.io.StringReader. s)))

(defn text-node-filter [node]
  (not (#{:script} (:tag node))))

(defn text-from-html
  "Modified version of net.cgrand.enlive-html with filter."
  [node-filter node]
  (cond
    (string? node) node
    (and (xml/tag? node) (node-filter node)) (apply str (map #(text-from-html node-filter %) (:content node)))
    :else ""))

(defn word-filter [word]
  (and (< 1 (count word))
    (not (common-aux-words word))
    (not (re-matches #"^\d+$" word))))

(defn split-keywords [text]
  (->> text
    (re-seq #"[\p{L}\p{N}]+")
    (map cstr/lower-case)))

(defn digest-keywords [text]
  {:pre (string? text)}
  (->> text
    split-keywords
    (filter word-filter)
    groom-keywords
    frequencies
    (map #(into [] (reverse %)))
    (sort (comp - compare))
    (map second)
    (take 60)))

(defn get-headings [html-struct]
  (let [get-headers #(groom-keywords (map html/text (html/select html-struct %)))]
    {:0 (get-headers [:head :title])
     :1 (get-headers [:body :h1])
     :2 (get-headers [:body :h2])}))

(defn digest-text [text]
  {:tags (digest-keywords text)})

(defn digest-html
  "Extract some words that hopefully would characterize given html text.
  html-struct is one returned by html/html-resource"
  [html-in]
  (let [html-struct (html/html-resource html-in)
        body (first (html/select html-struct [:body]))]
    {:keywords (get-keywords body)
     :tags     (->> body
                 (text-from-html text-node-filter)
                 digest-keywords)
     :headings (get-headings html-struct)}))

(defn keyword-set
  "Make set of normalized keywords to be used in lookup index."
  [digested-html url] ; TODO Also search cached html text
  (into #{}
    (concat
      (mapcat digest-keywords (flatten (vals (:headings digested-html))))
      (:keywords digested-html)
      (:tags digested-html)
      (digest-keywords url))))

#_(let [d (readmarks.analyse/digest-html (java.net.URL. "http://www.nasa.gov/"))]
  (pprint [(apply str (interpose " | " (take 12 (mapcat second (sort (d :headings))))))
           (apply str (interpose ", " (take 20 (d :tags))))]))
