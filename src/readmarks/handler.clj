(ns readmarks.handler
  (:use compojure.core
        readmarks.layout
        readmarks.links
        readmarks.links
        readmarks.link-views
        readmarks.routes)
  (:require [ring.middleware.defaults :as ring-defaults]
            [compojure.route :as route]
            [ring.util.response :as rr]
            [ring.middleware.resource]
            [ring.middleware.content-type]
            [ring.middleware.not-modified]
            [cemerick.friend :as friend]
            [cemerick.friend.openid :as openid]
            [cemerick.url :as curl]
            readmarks.spider
            readmarks.auth.login
            ring.util.anti-forgery
            [clojure.data.json :as json]
            [clojure.string :as cstr]
            (cemerick.friend [workflows :as workflows])
            readmarks.logger
            readmarks.executor))

(readmarks.logger/deflogger)

(defn get-user-id [req]
  {:post [(<= 0 %)]}
  (get-in req [::context :user-id]))

(defn get-default-uri [request]
  ; TODO (cleanup) Decide whether we need this switch (now there's explicit link to link list)
  #_(if (get-user-id request) "/links" "/")
  "/")

(defn link-details [req link-struct]
  (page {:home-link (get-default-uri req) :page-id :link-details}
    (link-details-view link-struct)))

(defn addition-page [db req page-id & add-forms]
  (let [url (get-in req [:query-params "url"])]
    (if-let [doc (link-get db (get-user-id req) url)]
      (page {:home-link (get-default-uri req) :page-id page-id}
        [:div
         [:p.text-warning "This URL is already added."]
         (link-details-view doc)])
      (page {:home-link (get-default-uri req) :page-id page-id}
        (link-add-form url add-forms)))))

(defn filter-links [db req query limit]
  (link-list db (get-user-id req) query limit))

(defn r-links [db req q]
  {:status 200
   :headers {"Content-Type" "application/json; charset=UTF-8"} ; application/json
   :body (json/write-str (filter-links db req q 50))})

(defn home-page [req]
  (page {:home-link (get-default-uri req) :page-id :home}
    [:p "Simple unsocial URL bookmarking service. "
     [:a {:href "http://vooza.com/videos/what-does-vooza-do/"} "Watch explainer video!"]]
    [:ul
     [:li "Save any URLs"]
     [:li "Have your URLs annotated automatically"]
     [:li "Lookup them later"]]
    #_(google-openid-notice)))

(defn fetch-update-link [db executor ctx id url]
  {:pre (<= 0 id)}
  (readmarks.executor/run-task executor
    #(link-update! db (:user-id ctx) id (readmarks.spider/digest-url url))))

(defn not-found-response [message]
  (-> (rr/not-found message)
    ;; Octet stream wih 404 may confuse browser.
    (rr/content-type "text/html; charset=utf-8")))

(defn link-not-found-response [link-id]
  (not-found-response (str "Link #" link-id " is not found.")))

(defn handle-link-fetch-update [db executor req id]
  (let [ctx (::context req)
        url (:url (link-get-by-id db (:user-id ctx) id []))]
    (if url
      (do
        (fetch-update-link db executor ctx id url)
        (rr/redirect (str "/links/" id)))
      (link-not-found-response id))))

(defn site-routes [db executor]
  (routes
    (GET "/" req
      (home-page req))

    (context links-path []
      (GET "/" [q :as req]
        (let [limit 70]
          (page {:home-link (get-default-uri req) :page-id :link-list}
            (link-list-view {:query q
                             :limit limit}))))
      (POST "/" [url :as req] ; Add link action
        (let [user-id (get-user-id req)]
          (let [id (link-add! db user-id url)]
            (fetch-update-link db executor (::context req) id url)))
        (rr/redirect links-path))
      (DELETE "/:id" [id :as req]
        (let [res (link-delete! db (get-user-id req) (Long/parseLong id))]
          (.debug logger "Deletion result {}" res)
          (rr/response (str res))))
      (GET "/add" req ; Add link view
        (addition-page db req :link-add (bookmarklet-div req)))
      (GET "/add/bookmarklet" req
        (.debug logger (str req))
        (addition-page db req :bookmarklet))
      (GET "/:id" [id :as req]
        (if-let [l (link-get-by-id db (get-user-id req) (Long/parseLong id) [:digest])]
          (link-details req l)
          (link-not-found-response id)))
      (POST "/:id/update" [id :as req]
        (handle-link-fetch-update db executor req (Long/parseLong id))))

    (GET auth-login-path req
      (page {:home-link (get-default-uri req) :page-id :login}
        (readmarks.auth.login/login-page req)))
    (GET "/auth/logout" req
      (friend/logout* (rr/redirect (str (:context req) "/"))))

    (GET api-links-path [q :as req]
      (r-links db req q))

    (route/not-found "Not Found")))

(defn handle-new-user [req handler data-source]
  (let [credentials (-> (:params req)
                      (select-keys [:username :password])
                      (update :username cstr/trim))]
    (if (create-user-u-p data-source credentials)
      (handler (-> req
                 (assoc :uri auth-login-path)
                 (assoc :body nil)
                 (assoc :params credentials)
                 ;; Usename is updated, but Friend prefers to get it from form-params.
                 (dissoc :form-params)))
      (rr/redirect
        (str auth-login-path "?"
          (curl/map->query
            {:username-taken "y"
             :username (:username credentials)}))))))

(defn wrap-new-user [handler uri-path-regex data-source]
  (fn [req]
    (if (re-matches uri-path-regex (:uri req))
      (handle-new-user req handler data-source)
      (handler req))))

(defn require-auth [handler uri-path-regex]
  (fn [req]
    (if (re-matches uri-path-regex (:uri req))
      (friend/authenticated (handler req))
      (handler req))))

(defn wrap-user-id
  "Sets sequential user id based on authentication."
  [handler]
  (fn [request]
    (handler (assoc-in request [::context :user-id]
               (get (friend/current-authentication request) :user-id)))))

(defn wrap-request-log [handler]
  (fn [request]
    (.debug logger "Request {}" request)
    (handler request)))

(defn app [{:keys [:db :executor :session-store]}]
  (-> (site-routes db executor)
    wrap-user-id
    (require-auth (re-pattern (str "^(" links-path "|" api-path ").*"))) ; No roles, just separate personal links from different users.
    ring.middleware.anti-forgery/wrap-anti-forgery
    (friend/authenticate
      {:allow-anon? true
       :default-landing-uri links-path
       :login-uri auth-login-path
       :workflows [(workflows/interactive-form
                     :credential-fn (partial find-user-by-credentials db))
                   (openid/workflow
                     :openid-uri "/auth/openid"
                     :credential-fn (partial user-by-openid db))]})
    (wrap-new-user (re-pattern (str "^(" auth-new-user-path ").*")) db)
    wrap-request-log
    (ring-defaults/wrap-defaults

      ; FIXME cover login by anti-forgery (create session automatically on reaching login page, session_presence != authenticated)

      ; XXX Exempt friend's openid auth form from anti-forgery (see also wrap-anti-forgery above):
      (-> ring-defaults/site-defaults
        (assoc-in [:security :anti-forgery] false)
        (assoc-in [:session :store] session-store)))))
