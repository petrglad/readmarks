(ns readmarks.lab)

(ns readmarks.lab.a)
(defn f [] "Called lab.a.f")

(ns readmarks.lab.b (require readmarks.lab.a))
(defn f [] (str "Called lab.b.f, lab.a.f->(" (readmarks.lab.a/f) ")"))

(ns readmarks.lab.c (require readmarks.lab.b))
(defn f [] (str "Called lab.c.f, lab.b.f->(" (readmarks.lab.b/f) ")"))


(readmarks.lab.c/f)

; -------
(comment
  (require '[clojure.java.jdbc :as j])
  (defn get-db [] (:db server))
  (j/query (get-db)
    ["select * from links"]
    :row-fn :cost)
  (j/with-db-connection
    [c (get-db)]
    (-> (:connection c)
      (.getMetaData)
      (.getTables nil nil nil (into-array ["TABLE" "VIEW"]))
      (j/result-set-seq))))

; -------
#_(:import org.apache.commons.codec.digest.DigestUtils
  org.apache.commons.codec.binary.Base64)