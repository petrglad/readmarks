(let [generated-resources "target/resources"
      generated-js-base (str generated-resources "/public/js")]

  (defproject
    readmarks "0.2.3-SNAPSHOT"
    :description "Unsocial URL bookmarking service."
    :url "https://readmarks.net/"

    ; List of linters http://blog.mattgauger.com/blog/2014/09/15/clojure-code-quality-tools
    :plugins [[lein-cljsbuild "1.1.7"]]
;    :middleware [leiningen.cljsbuild/activate]

    :dependencies [[org.clojure/clojure "1.10.1"]
                   [environ "1.1.0"]
                   [net.readmarks/compost "0.2.0"]

                   [ring "1.7.1" :exclusions [ring/ring-jetty-adapter]]
                   ; [javax.servlet/servlet-api "2.5"]
                   [compojure "1.6.1"]
                   [ring/ring-defaults "0.3.2"]
                   [ring/ring-ssl "0.3.0"]
                   [info.sunng/ring-jetty9-adapter "0.12.4"]
                   ; [http-kit "2.1.16"]
                   [com.cemerick/friend "0.2.3"]
                   [crypto-password "0.2.1"]
                   [hiccup "1.0.5"]
                   [enlive "1.1.6"]
                   [clj-http "3.10.0"]

                   [honeysql "0.9.5"]
                   [org.clojure/java.jdbc "0.7.9"]
                   ; [nilenso/honeysql-postgres "0.2.2"]
                   ; [cantata "0.1.17"]
                   [postgre-types "0.0.4" :exclusions [postgresql]]
                   [org.postgresql/postgresql "42.2.6"]
                   [com.zaxxer/HikariCP "3.3.1"]
                   [org.flywaydb/flyway-core "5.2.4"]

                   [org.slf4j/slf4j-api "1.7.28"]
                   [org.slf4j/log4j-over-slf4j "1.7.28"]
                   [ch.qos.logback/logback-classic "1.1.8"]

                   [clojure-opennlp "0.5.0"]
                   [crouton "0.1.2"]
                   [clj-tagsoup "0.3.0" :exclusions [org.clojure/clojure]]

                   [commons-codec/commons-codec "1.13"]
                   [org.clojure/data.json "0.2.6"]
                   ; [org.clojure/math.numeric-tower "0.0.2"]

                   [org.clojure/core.typed "0.6.0"]
                   [prismatic/schema "1.1.12"]
                   [com.cemerick/url "0.1.1"]

                   ; ClojureScript
                   [org.clojure/clojurescript "1.10.520"]
                   ; [hiccups "0.3.0"]
                   [jayq "2.5.5"]
                   [reagent "0.8.1"] ;; :exclusions [cljsjs/react]
                   ; [sablono ...]
                   [cljs-hash "0.0.2"]
                   ; [bidi "1.18.10"]
                   ; [com.domkm/silk "0.0.4"]

                   ; Dependency conflicts resolution (do not need these directly):
                   [slingshot "0.12.2"]
                   [org.apache.httpcomponents/httpclient "4.5.9"]
                   [instaparse "1.4.10"]
                   [com.fasterxml.jackson.core/jackson-core "2.10.0.pr1"]
                   [org.clojure/core.cache "0.7.2"]
                   [org.clojure/core.memoize "0.7.2"]
                   [org.clojure/tools.reader "1.3.2"]]

    :source-paths ["src" "src-cljc" "src-cljs"]
    :resource-paths ["resources" ~generated-resources]

    :hooks [leiningen.cljsbuild]

    :repl-options {:init-ns  user}
    :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]

    ;:plugins [[lein-ring "0.8.3"]]
    ;:ring {:handler readmarks.handler/app}

    :prep-tasks ["javac"
                 "compile"]

    :cljsbuild {:builds
                    {:main
                      {:source-paths ["src-cljs" "src-cljc"]
                       :compiler     {:output-to  ~(str generated-js-base "/main.js")
                                      :asset-path "/js"
                                      ; To have same starting file with 'none' optimizations:
                                      :main       "readmarks.main"}}}}
    :profiles
    {:dev
     {:source-paths ["dev"]
      :dependencies [[ring-mock "0.1.5"]
                     [org.clojure/tools.trace "0.7.10"]
                     [org.clojure/java.classpath "0.3.0"]
                     [org.clojure/tools.namespace "0.3.1"]
                     [org.clojure/tools.analyzer.jvm "0.7.2"]
                     [org.clojure/core.logic "0.8.11"]
                     [org.clojure/core.unify "0.5.7"]
                     [com.cemerick/pomegranate "1.1.0"]
                     [org.clojure/tools.nrepl "0.2.13"]
                     [clojure-complete "0.2.5"]]
      :cljsbuild    {:builds
                     {:main
                      {:compiler {:optimizations :none
                                  ;:optimizations :advanced ; :none :whitespace :simple
                                  :output-dir    ~generated-js-base ; Required for 'none' optimizations
                                  :pretty-print  true}}}}}
     :uberjar
     {:aot       :all
      :cljsbuild {:builds
                  {:main
                   {:compiler {; https://github.com/google/closure-compiler/blob/master/contrib/externs/
                               :externs       ["dev/resources/jquery-3.1-extern.js"]
                               :optimizations :advanced
                               :pretty-print  false}}}}}}

    :main readmarks.main))
